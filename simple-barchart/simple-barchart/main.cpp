// stl
#include <utility> // std::pair
// Qt5
#include <QList>        // QList
#include <QApplication> // QApplication
// QtCharts
#include <QtCharts/QBarSet>    // QBarSet
#include <QtCharts/QBarSeries> // QBarSeries
#include <QtCharts/QChartView> // QChart, QChartView

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);

  // 1. QBarSet
  QList<QtCharts::QBarSet *> vSets{
      new QtCharts::QBarSet("Preprocessing"),
      new QtCharts::QBarSet("FeatureExtraction"),
      new QtCharts::QBarSet("Matching"),
      new QtCharts::QBarSet("Bundle"),
      new QtCharts::QBarSet("Dense")
  };
  *vSets[0] << 50  << 35;
  *vSets[1] << 100 << 150;
  *vSets[2] << 150 << 300;
  *vSets[3] << 125 << 250;
  *vSets[4] << 300 << 500;
  // 2. QBarSeries
  QtCharts::QBarSeries barSeries;
  barSeries.append(vSets);
  // 3. QChart
  auto pChart = new QtCharts::QChart;
  pChart->addSeries(&barSeries);
  // 4. QChartView
  QtCharts::QChartView view;
  view.setChart(pChart);
  view.show();

  return app.exec();
}
