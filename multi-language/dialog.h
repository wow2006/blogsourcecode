#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>

QT_FORWARD_DECLARE_CLASS(QLabel)

class Dialog : public QDialog
{
    Q_OBJECT

public:
    Dialog(QWidget *pParent = nullptr);

    ~Dialog() override;

private:
    QLabel *m_pLabel = nullptr;

};

#endif // DIALOG_H
