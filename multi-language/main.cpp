#include "dialog.h"

#include <QDebug>
#include <QTranslator>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator myappTranslator;

    QString path = QApplication::applicationDirPath();
    path.append("/languages/multi-language_ar.qm");
    if(myappTranslator.load(path)) {
      a.installTranslator(&myappTranslator);
    } else {
      qDebug() << "Can not load qm";
    }

    Dialog w;
    w.show();

    return a.exec();
}
