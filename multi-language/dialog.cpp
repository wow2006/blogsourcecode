#include "dialog.h"

#include <QLabel>

Dialog::Dialog(QWidget *parent) : QDialog(parent) {
  m_pLabel = new QLabel{tr("HelloWorld!"), this};
}

Dialog::~Dialog() = default;
